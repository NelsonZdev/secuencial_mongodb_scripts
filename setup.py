# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Sequential mongodb scripts',
    version='0.1.0',
    description='Solution to manual execution of js script files in mongo',
    long_description=readme,
    author='Nelson Zapata',
    author_email='stiven101998@gmail.com',
    url='https://gitlab.com/NelsonZdev/secuencial_mongodb_scripts.git',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
