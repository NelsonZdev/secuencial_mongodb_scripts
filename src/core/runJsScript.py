from miscellaneous import systemArgs as sysArgs
from re import compile
from os import listdir, path
from subprocess import check_output, CalledProcessError
from sys import exit, argv
from pathlib import Path
import json

NUMBERS = compile(r'(\d+)')


def numerical_short_key(value):
    parts = NUMBERS.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


class RunJs:
    __SYS_ARGS = sysArgs.Args()
    __script_path = ''
    __json_status_script = {'status': []}
    __execution_history_path = ''
    __insecure_functions = False
    __ignore_errors = False
    __json_file_name = ''
    __ignored_scripts = []
    __actual_build = 0
    __executed_in_build = []
    __mongo_port = '27017'
    __docker_container_name= ''

    def __init__(self):
        self.__get_assignation_args_value(self.__SYS_ARGS)
        self.__get_execution_args_value(self.__SYS_ARGS)

        if not self.__insecure_functions:
            self.__ignored_scripts = self.__check_history()

        if self.__insecure_functions:
            print("\U0001F6C7\U0001F6E1\U0001F6C7 Insecure functions Active")

        if self.__ignore_errors:
            print('\U0001F6C7 Verbose error messages has been ignored')

        print('History of executions has been saved in {file}'.format(file=self.__execution_history_path))
        self.__scripts_filter()

    def __check_history(self):
        scripts_completed = []

        "If file not exist return []"
        if not path.isfile('{path}/{file}'.format(path=self.__execution_history_path, file=self.__json_file_name)):
            return scripts_completed

        with open("{}/{}".format(self.__execution_history_path, self.__json_file_name)) as json_file:
            try:
                data = json.load(json_file)
                for status in data['status']:
                    if status['completed']:
                        name = status['name']
                        associated_build = status['associatedBuild']
                        scripts_completed.append(name)
                        self.__executed_in_build.append({name: associated_build})
            except json.decoder.JSONDecodeError as e:
                print("Void file; error: ", e)

        return scripts_completed

    def __scripts_filter(self):
        scripts_route_content = sorted(listdir(self.__script_path),
                                       key=numerical_short_key)  # Return all files in folder
        """Add only js files to array"""
        script_filter = []
        for script in scripts_route_content:
            if path.splitext(self.__script_path + "/" + script)[-1] == ".js":  # Find only js files
                script_filter.append(script)

        if len(script_filter) == 0:
            print('Scripts not found')
            exit(0)

        self.__write_json_file(script_filter)

    def __run_script(self, script):
        """ If script has been executed correctly ignore it """
        script_coincidence = 0
        for ignored in self.__ignored_scripts:
            if script == ignored:
                print("Ignored", script)
                script_coincidence += 1
        if self.__insecure_functions:
            """ Always run all scripts """
            script_coincidence = 0

        if script_coincidence == 0:
            if self.__docker_container_name == '':
                check_output(["mongo", f"localhost:{self.__mongo_port}", f"{self.__script_path}/{script}"])
            else:
                # Copy
                check_output(["docker", "cp", f"{self.__script_path}/{script}",
                              f"{self.__docker_container_name}:/{script}"])
                print('Script copied to container \u2713')
                # Run
                check_output(["docker", "exec", f"{self.__docker_container_name}",
                              "mongo", f"localhost:{self.__mongo_port}", f"/{script}"])
                print('Command executed...\n')

    def __write_json_file(self, filter_scripts: []):
        # Make a jsonFile
        with open(path.join(self.__execution_history_path, self.__json_file_name), "w") as script_sequence:
            for index, script in enumerate(filter_scripts):
                print("\n\u25B6 {} \u2BB7".format(script))
                print("--** Program output **--\n")
                try:
                    self.__run_script(script)
                    build_number = self.__associated_value(script)

                    self.__json_status_script['status'].append({
                        'name': script,
                        'completed': True,
                        'associatedBuild': build_number
                    })
                    message = "Runs: OK \u2713" if build_number == self.__actual_build else \
                        f"Associated to build: {build_number}"
                    print(message)
                except CalledProcessError as error:
                    error_message = self.__get_mongo_error(error)

                    self.__json_status_script['status'].append({
                        'name': script,
                        'completed': False,
                        'errorCode': error.returncode,
                        'log': error_message
                    })
                    print("Runs: FAILED \u274C")
                    print('Error code:', error.returncode, '\n Output:\n', error_message)
                    if not self.__ignore_errors:
                        print("\n !!!--- LOG MESSAGE ---!!!")
                        print(error.output.decode("utf-8"))
                        print("!!!___________________!!!")
                        print("\n--********************--")
                    if not self.__insecure_functions:
                        json.dump(self.__json_status_script, script_sequence)
                        print("\n************* EXIT with error ************\n")
                        exit(error.returncode)

                print("\n--********************--")
            json.dump(self.__json_status_script, script_sequence)

    def __get_mongo_error(self, error):
        error_list = "{}".format(error.output.decode("utf-8")).split()
        for word_index, word in enumerate(error_list):
            if word == 'Error:' and error_list[word_index + 1] == 'Authentication':
                return self.__detail_error(error_list, word_index)
            elif word == 'Error:':
                return "{} {}".format(word, error_list[word_index + 1])
            elif word == 'exception:':
                return self.__detail_exception(error_list, word_index)

        return error_list

    def __get_assignation_args_value(self, system_args: type(sysArgs.Args)):
        self.__script_path = system_args.get_assignation_args_value('-scriptPath', default=argv[1])
        self.__execution_history_path = \
            system_args.get_assignation_args_value('-executionHistory', default=Path.home())
        self.__actual_build = system_args.get_assignation_args_value('-jBuild', default=0)
        self.__json_file_name = system_args.get_assignation_args_value('-jsonName', default='script_sequence.json')
        self.__mongo_port = system_args.get_assignation_args_value('-port', default='27017')
        self.__docker_container_name = system_args.get_assignation_args_value('-dockerContainer', default='')
        if self.__json_file_name != 'script_sequence.json':
            self.__json_file_name = "{name}{extension}".format(name=self.__json_file_name, extension='.json')

    def __get_execution_args_value(self, system_args: type(sysArgs.Args)):
        self.__insecure_functions = system_args.get_execution_args_value('--noSecure')
        self.__ignore_errors = system_args.get_execution_args_value('--ignoreErrors')

    def __associated_value(self, key: str):
        for _object in self.__executed_in_build:
            if key in _object:
                return _object[key]
        return self.__actual_build

    @staticmethod
    def __detail_error(error_list: list, word_index: int):
        return " ".join(error_list[word_index + 1: word_index + 3])

    @staticmethod
    def __detail_exception(error_list: list, word_index: int):
        return " ".join(error_list[word_index + 2: word_index + 7])
