from core import runJsScript as jS
from miscellaneous import systemArgs, consoleMessages
from sys import argv

ARGS = tuple(argv)


@consoleMessages.start_program
def app():
    jS.RunJs()


if __name__ == "__main__":
    if len(ARGS) != 2:
        sys_args = systemArgs.Args()
        sys_args.check_system_syntax()
    app()
