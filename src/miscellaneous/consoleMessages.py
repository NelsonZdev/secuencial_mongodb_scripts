from functools import wraps


def start_program(_func=None, *, app_name='Program'):
    def decorator_start_program(func):
        @wraps(func)  # Preserve information about the original function
        def mask_func(*args, **kwargs):
            print(f"\n********* {app_name} has been started *********\n")
            result = func(*args, **kwargs)
            print(f"\n************* End of {app_name} *************\n")
            return result
        return mask_func

    if _func is None:
        return decorator_start_program
    else:
        return decorator_start_program(func=_func)
