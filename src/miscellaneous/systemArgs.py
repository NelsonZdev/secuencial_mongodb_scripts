from sys import argv


class Args:
    """ Get _args passed by user when run this program:
        The first python arg is ignored because is program execution"""
    __SYSTEM_ARGS = tuple(argv)

    __CHECK_ARGS = (
        '-scriptPath',
        '-executionHistory',
        '-jBuild',
        '--noSecure',
        '--ignoreErrors',
        '-jsonName',
        '-port',
        '-dockerContainer'
    )

    def get_assignation_args_value(self, flag: str, *, default=None, required=False):
        flag_type = self.__flag_type(flag)
        if flag_type != 'assignation':
            raise ValueError(
                "{flag} is not an assignation flag".format(flag=flag))
        value = self.__get_args_value(
            flag=flag, flag_type=flag_type, required=required)
        if value is False:
            return default
        return value

    def get_execution_args_value(self, flag: str, *, required=False):
        flag_type = self.__flag_type(flag)
        if flag_type != 'execution':
            raise ValueError(
                "{flag} is not an execution flag".format(flag=flag))
        return self.__get_args_value(flag=flag, flag_type=flag_type, required=required)

    def __get_args_value(self, flag: str, flag_type: str, *, required=False):
        self.__flag_exist(flag)
        """ Get value if the user passed argument """
        for index, params in enumerate(self.__SYSTEM_ARGS):
            if flag_type == 'assignation' and flag == params:
                return self.__SYSTEM_ARGS[index + 1]
            if flag_type == 'execution' and flag == params:
                return True
        if required:
            raise ValueError("Need use -from and -to flags\n"
                             "usage: App.py [-scriptPath SCRIPT[")
        return False

    def check_system_syntax(self):
        """ :return error if the first argument is not a --Backup or Restore flag"""
        if self.__SYSTEM_ARGS[1] != self.__CHECK_ARGS[0]:
            raise ValueError(
                "\u274C\u274C '{}' command not found \u274C\u274C\n"
                "Please use '-scriptPath' as first argument".format(
                    self.__SYSTEM_ARGS[1])
            )
        for param in self.__SYSTEM_ARGS:
            if self.__flag_type(flag=param) is not None:
                self.__flag_exist(param)

    def __flag_exist(self, param):
        """ Check if flag exists if not run exception
            :return void str """
        for correct_flag in self.__CHECK_ARGS:
            if param == correct_flag:
                return ''

        raise SyntaxError("Invalid flag {}".format(param))

    @staticmethod
    def __flag_type(flag):
        if flag[0] == '-':
            if flag[1] == '-':
                return 'execution'
            return 'assignation'
        return None
