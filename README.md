# Secuencial_mongoDB_scripts

Run automatically the mongo js scripts

---

## Requisites

1. Linux or WSL
2. Python > 3.5

## Using

### 1. Clone from a repository:

```
https://gitlab.com/NelsonZdev/secuencial_mongodb_scripts.git
```

### 2. Run sh or python

`sh [run.sh]/[python3 src/app.py] -scriptPath 'Your scripts path'` **Required**

###OR

`sh [run.sh]/[python3 src/app.py] 'Your scripts path'` **Required**

### Options

- #### Only if `'-scriptPath'` is used

  **Assignation params**

  - `-executionHistory 'path'` Path where your scripts execution history is going to be saved **Optional**
  - `-jBuild 'number'` Associate a build number to script execution **Optional**
    - By default, is 0
  - `-jsonName 'name'` Assign name to json file **Optional**
  - `-port 'number'` Use custom port for mongodb execution command **Optional**
  - `-dockerContainer 'name'` Execute mongodb command into a docker container, using the name of container **Optional**

  **Execution params**

  - `--noSecure` Runs non-secure actions: **Optional**

    - Ignore script execution history.
    - Continue even if the other scripts fail .

  - `--ignoreErrors` No verbose error Messages. **Optional**

- #### If `-scriptPath` not active use by default

  - `-executionHistory [user_home_path]`
  - `-jBuild [0]`
  - `--noSecure` **Disabled**
  - `--ignoreErrors` **Disabled**
  - `-jsonName script_sequence.json`
  - `-port 27017`
  - `-dockerContainer` **Disabled** 

