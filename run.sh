#!/bin/bash

echo "Running MongoDB scripts"

# Actual Folder
DIR=$(pwd)
RUNS=0;

python3 ./src/App.py "$1"
RUNS=$?

# Return origin folder
cd "$DIR" || exit 1
exit $RUNS;
